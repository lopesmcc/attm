var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver,
    serviceUrl,
    observers;

window.addEventListener('load', function() {
    bootstrap(function() {
        ifRecording(function() {
            ifServiceIsRunning(startObserving)
        });
    });
});

function bootstrap(callback) {
    getSetting("service_url", function (resp) {
        serviceUrl = resp;
        callback();
    });
}

function ifRecording(callback) {
    getSetting("state", function (state) {
        if (state === 'recording') {
            callback();
        }
    });
}

function ifServiceIsRunning(callback) {
    // TODO
    //$.get(serverURL + "/isalive", callback);
    callback();
}

function observeMutations(mutations, recordStream) {
    //var name = recordStream['name'],
    //    escaped = recordStream['escaped'];
    console.log(mutations);
    console.log(recordStream);
}

function startObserving() {
    getSetting('record_streams', function(recordStreams) {
        if (typeof recordStreams != 'undefined') {
            observers = new Array();
            for (var recordStream in recordStreams) {
                observeRecordStream(recordStream);
            }
            console.log('Observing ' + observers.length() + ' record streams.');
        } else {
            console.log('No record streams available to observe.');
        }
    });
}

function observeRecordStream(recordStream) {
    if ('selector' in recordStream && 'name' in recordStream) {
        var observer = new MutationObserver(function(mutations) {
            observeMutations(mutations, recordStream);
        });
        $.each($(recordStream['selector']),function(node) {
            observer.observe(node, { childList: true, subtree: true});
        });
        observers.push(observer);
    }
}

function stopObserving() {
    $.each(observers, function(observer) {
        observer.disconnect();
    });
    observers = null;
}

function getSetting(name, callback) {
    chrome.extension.sendRequest({type: "get", name: name}, callback);
}