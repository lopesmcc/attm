function doRecord() {
    simpleGet('/record', function () {
        setState('recording');
        refreshPage();
    });
}

function doSave() {
    simpleGet('/save', function () {
        setState('idle');
        refreshPage();
    });
}

function doCancel() {
    simpleGet('/cancel', function () {
        setState('idle');
        refreshPage();
    });
}

function simpleGet(path, callback) {
    getSetting('service_url', function(service_url) {
        $.get(service_url + path, callback);
    });
}

function toggleSettings() {
    $(this).text($(this).text() == 'Show settings' ? 'Hide settings' : 'Show settings');
    $('.settings').toggle();
}

function setState(state) {
    setSetting('state', state);
    $('state').hide();
    $(state).show();
}

function getSetting(name, callback) {
    chrome.extension.sendRequest({type: "get", name: name}, callback);
}

function setSetting(name, value, callback) {
    chrome.extension.sendRequest({type: "set", name: name, value: value}, callback);
}

function refreshPage(callback) {
    chrome.tabs.getSelected(null, function(tab) {
        var code = 'window.location.reload();';
        chrome.tabs.executeScript(tab.id, {code: code});
        if (callback) {
            callback();
        }
    });
}