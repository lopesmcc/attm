localStorage['service_url'] = 'http://localhost:5891';
localStorage['state'] = 'idle';

chrome.extension.onEvent.addListener(
    function(request, sender, sendResponse) {
        switch (request.type) {
            case "get":
                var data = {};
                if (request.name.constructor == Array) {
                    for (var i = 0; i < request.name.length; i++) {
                        data[request.name[i]] = localStorage[request.name[i]];
                        sendResponse(data);
                    }
                } else {
                    sendResponse(localStorage[request.name]);
                }
                break;
            case "set":
                localStorage[request.name] = request.value;
                break;
            default:
                console.log("Invalid request " + request);
                break;
        }
    });
